package hackathon.weiqiapp.com.chime2abys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class GuideActivity extends Activity
{

    private ViewPager mPager;
    private LinearLayout mDotsLayout;
    private ImageButton mBtn;
    private TextView txtBtnSkip;
    private TextView txtBtnSignup;
    private TextView txtBtnLogin;

    private List<View> viewList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scenarios);

        mPager = (ViewPager)findViewById(R.id.review_scenario_viewpage);
        mDotsLayout = (LinearLayout)findViewById(R.id.guide_dots);
        //mBtn = (ImageButton)findViewById(R.id.guide_btn);
        txtBtnSkip = (TextView)findViewById(R.id.guide_skip);
        txtBtnSignup = (TextView)findViewById(R.id.guide_signup);
        txtBtnLogin = (TextView)findViewById(R.id.guide_login);

        initPager();

        mPager.setAdapter(new ViewPagerAdapter(viewList));
        mPager.setOnPageChangeListener(new OnPageChangeListener()
        {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                for (int i = 0; i < mDotsLayout.getChildCount(); i++) {
                    if(i == arg0){
                        mDotsLayout.getChildAt(i).setSelected(true);
                    } else {
                        mDotsLayout.getChildAt(i).setSelected(false);
                    }
                }

                /*
                if(arg0 == mDotsLayout.getChildCount()-1){
                    mBtn.setVisibility(View.VISIBLE);
                } else {
                    mBtn.setVisibility(View.GONE);
                }
                */
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

        txtBtnSkip.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View view) {

                openHome();
            }
        });

        txtBtnSignup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                openHome();
            }
        });


        txtBtnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                openHome();

            }
        });


        /*
        mBtn.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                openHome();
            }
        });

        */


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.menu_guide, menu);
        return true;
    }

    private void initPager(){
        viewList = new ArrayList<View>();
        int[] images = new int[] { R.drawable.onescenario800x450, R.drawable.onescenario800x450, R.drawable.onescenario800x450, R.drawable.onescenario800x450, R.drawable.onescenario800x450};
     //   String[] strTitle = {getString(R.string.guide_titile_merchant), getString(R.string.guide_titile_foodie), getString(R.string.guide_titile_chef)};
       // String[] strSlogan = {getString(R.string.guide_slogan_merchant), getString(R.string.guide_slogan_foodie), getString(R.string.guide_slogan_chef)};
        for (int i = 0; i < images.length; i++) {
         //   viewList.add(initView(images[i],strTitle[i],strSlogan[i]));
        }
        initDots(images.length);
    }

    private void initDots(int count){
        for (int j = 0; j < count; j++) {
            mDotsLayout.addView(initDot());
        }
        mDotsLayout.getChildAt(0).setSelected(true);
    }

    private View initDot(){
        return LayoutInflater.from(getApplicationContext()).inflate(R.layout.layout_dot, null);
    }

    private View initView(int res, String title, String slogan){
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.item_guide, null);
        ImageView imageView = (ImageView)view.findViewById(R.id.iguide_img);

        TextView txtviewTitle = (TextView)view.findViewById(R.id.iguide_title);
        TextView txtviewSlogan = (TextView)view.findViewById(R.id.iguide_slogan);

        imageView.setImageResource(res);
        txtviewTitle.setText(title);
        txtviewSlogan.setText(slogan);

        //Animation fadeIn = AnimationUtils.loadAnimation(GuideActivity.this, R.anim.fade_in);
        //imageView.startAnimation(fadeIn);
        //fadeIn.start();

        return view;
    }

    private void openHome(){
        Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_righttoleft,R.anim.exit_righttoleft);
        finish();
    }

    class ViewPagerAdapter extends PagerAdapter {

        private List<View> data;


        public ViewPagerAdapter(List<View> data)
        {
            super();
            this.data = data;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return data.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            // TODO Auto-generated method stub
            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // TODO Auto-generated method stub
            container.addView(data.get(position));
            return data.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(data.get(position));
        }

    }

}