package hackathon.weiqiapp.com.chime2abys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class InputCodeActivity extends AppCompatActivity {

    private TextView txtBtnSubmitCode, txtCode;
    private final static String TAG_LOG = "LOG:";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_code);


        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        txtBtnSubmitCode = (TextView)findViewById(R.id.submit_code_txt);
        txtCode = (TextView)findViewById(R.id.challenge_code_input);

        txtBtnSubmitCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitCode();
            }
        });


    }

    private void submitCode() {

        String codeInput = txtCode.getText().toString();

        Log.d(TAG_LOG, "codeInput:"+codeInput+"<--");
        if (TextUtils.isEmpty(txtCode.getText())) {

            Toast.makeText(this, "Show me the code!", Toast.LENGTH_SHORT).show();
            Log.d(TAG_LOG, "show me the code");


            return;
        }
        else {
            if (isValidInviteCode(codeInput)) {

                Log.d(TAG_LOG, "code is valid!");

                Intent intent = new Intent(getApplicationContext(), LoginPhoneTwitterActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_righttoleft, R.anim.exit_righttoleft);
                finish();
                return;
            } else {
                Log.d(TAG_LOG, "wrong code!");
                Toast.makeText(this, "You got the wrong code!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    public boolean isValidInviteCode(String code){
        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_input_code, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
