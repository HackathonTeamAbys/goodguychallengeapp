package hackathon.weiqiapp.com.chime2abys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import io.fabric.sdk.android.Fabric;

public class LoginPhoneTwitterActivity extends Activity {


    private final static String TAG_LOGTWITTER = "LOGINTWITTER:";

    private TwitterLoginButton loginButton;
    private Button btn_submit_phonenum;
  //  private TextView status;
    private TextView txtPhoneNum;

    public String userName="";

    int uiOptions;
    View decorView;

    private boolean isReadyForLoginWithTwitter=false;

    private EditText inputPhoneText;

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "HX0FCyxBjbBYMhBy5CD8Iz4hk";
    private static final String TWITTER_SECRET = "8DiMHBFkEg6RnTgrlswoQMzCGccAkBBa96OszbbJf9mJSK81gt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_submit_phone_twitter);

        decorView = getWindow().getDecorView();
        // Hide the status bar.
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);


        txtPhoneNum = (TextView)findViewById(R.id.phonenumber);
        btn_submit_phonenum = (Button)findViewById(R.id.btn_submit_phonenum);

        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
       // status = (TextView)findViewById(R.id.status);
       // status.setText("Status: Ready");

        loginButton.setVisibility(View.INVISIBLE);
        btn_submit_phonenum.setVisibility(View.VISIBLE);


        btn_submit_phonenum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPhoneNumber();
            }
        });

            loginButton.setCallback(new LoginHandler());

    }

    public void checkPhoneNumber(){
        String phoneInput = txtPhoneNum.getText().toString();

        if (TextUtils.isEmpty(txtPhoneNum.getText())) {

            Toast.makeText(this, "Show me your phone number!", Toast.LENGTH_SHORT).show();
            Log.d(TAG_LOGTWITTER, "show me the phone number");

        }
        else {
            if (isValidInviteCode(phoneInput)) {

                uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);


                isReadyForLoginWithTwitter=true;
                btn_submit_phonenum.setVisibility(View.INVISIBLE);
                loginButton.setVisibility(View.VISIBLE);
                txtPhoneNum.setKeyListener(null);

                Log.d(TAG_LOGTWITTER, "phone num is valid!");

            } else {
                Log.d(TAG_LOGTWITTER, "wrong phone num!");
                Toast.makeText(this, "!Check your phone num!", Toast.LENGTH_SHORT).show();

            }
        }
    }

    public boolean isValidInviteCode(String phonenum){

        return true;

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginButton.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class LoginHandler extends Callback<TwitterSession> {
        @Override
        public void success(Result<TwitterSession> twitterSessionResult) {




            uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);


         //   String output = "Status: " +
           //         "Your login was successful " +
             //       twitterSessionResult.data.getUserName() +
               //     "\nAuth Token Received: " +
                 //   twitterSessionResult.data.getAuthToken().token;

            //status.setText(output);

            //userName = twitterSessionResult.data.getUserName();

            Intent intent = new Intent(getApplicationContext(), StartChallengeActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_righttoleft,R.anim.exit_righttoleft);
            finish();

            //composeATweet();

        }

        @Override
        public void failure(TwitterException e) {
           // status.setText("Status: Login Failed");
        }
    }




}
