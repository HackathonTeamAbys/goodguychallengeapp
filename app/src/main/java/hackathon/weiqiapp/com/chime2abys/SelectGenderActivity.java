package hackathon.weiqiapp.com.chime2abys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class SelectGenderActivity extends AppCompatActivity {
    private TextView txtBtnMale, txtBtnFemale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectgender);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        txtBtnMale = (TextView)findViewById(R.id.gender_male_text);
        txtBtnFemale = (TextView)findViewById(R.id.gender_female_text);

        txtBtnMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startMale();
            }
        });

        txtBtnFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startFemale();
            }
        });

    }

    private void startMale(){
        Intent intent = new Intent(getApplicationContext(), InputCodeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_righttoleft,R.anim.exit_righttoleft);
        finish();
    }


    private void startFemale(){
        Intent intent = new Intent(getApplicationContext(), ScenariosActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_righttoleft,R.anim.exit_righttoleft);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
