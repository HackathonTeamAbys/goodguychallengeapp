package hackathon.weiqiapp.com.chime2abys;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import io.fabric.sdk.android.Fabric;

public class ShareActivity extends Activity {

    private Button btn_share;

    private static final String TWITTER_KEY = "HX0FCyxBjbBYMhBy5CD8Iz4hk";
    private static final String TWITTER_SECRET = "8DiMHBFkEg6RnTgrlswoQMzCGccAkBBa96OszbbJf9mJSK81gt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        btn_share = (Button)findViewById(R.id.btn_share);


        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getBaseContext(), "Click the share!", Toast.LENGTH_SHORT).show();

                TweetComposer.Builder builder = new TweetComposer.Builder(getBaseContext())
                        .text("just setting up my Fabric.");
                builder.show();

            }
        });
    }

    public void shareToTwitter(){



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
